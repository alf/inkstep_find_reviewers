require 'elasticsearch'
require 'elasticsearch/dsl'
require 'ink_step/conversion_step'
require 'find_reviewers/version'

include Elasticsearch::DSL

module InkStep::Coko
  class MoreLikeThisStep < InkStep::ConversionStep

    def perform_step
      super
      
      source_file = find_source_file(regex: [/\.te?xt$/])
      source_file_extension = Pathname(source_file).extname
      source_file_name = Pathname(source_file).sub_ext ''
      source_file_path = File.join(working_directory, source_file)
      
      text = read_file(source_file_path)
      size = parameter(:size)
      year = parameter(:punctuation)
      source = parameter(:source)
      
      definition = search do
        query do
          bool do
            must do
              more_like_this do
                like text 
                fields [:title, :paperAbstract]
                analyzer :stop
                min_term_freq 1
                max_query_terms 7
              end
            end

            filter do
              range :year do
                gte year
              end
            end
          end
        end

        size size

        source source

        explain true
      end  

      host = ENV['ELASTICSEARCH_HOST'] || 'elasticsearch' # from .env or Docker service name

      log_as_step "Using Elasticsearch host #{host}"

      client = Elasticsearch::Client.new host: host
      
      result = client.search index: 'scholar', type: 'paper', body: definition.to_hash
      
      output_file = "#{source_file_name}_mlt.json"      
      output_file_path = File.join(working_directory, output_file)

      log_as_step "Writing papers like #{source_file} to #{output_file}"
      
      file = File.new(output_file_path, 'w+')
      File.write(file, JSON.generate(result['hits']))

      cleanup!(source_file_path)
      success!
    end

    def accepted_parameters
      {
        size: 'The number of similar papers to return',
        year: 'Only include papers published since this year',
        source: 'The source fields to include in the response',
      }
    end
    
    def default_parameter_values
      {
        size: 100,
        year: 1900,
        source: ['id', 'title', 'authors', 'year', 'keyPhrases', 'venue', 'journalName'],
      }
    end

    def required_parameters
      []
    end

    def version
      FindReviewers::VERSION
    end

    def self.description
      "Finds papers similar to the input text"
    end

    def self.human_readable_name
      "More Like This"
    end

    protected
    def cleanup!(file)
      File.delete(file)
    end

    def read_file(input_file)
      if input_file.respond_to? :read
        input_file.read
      elsif input_file.is_a?(String) && File.exist?(input_file)
        File.read(input_file)
      else
        raise "cannot read '#{input_file}'"
      end
    end
  end
end
