module FindReviewers
  if defined?(Rails)
    require 'find_reviewers/engine'
  else
    require 'find_reviewers/ink_step/coko/more_like_this_step'
  end
end
