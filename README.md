# InkStep - FindReviewers

This is a step gem that runs as a plugin for [the INK API](https://gitlab.coko.foundation/INK/ink-api).

### MoreLikeThisStep

This step takes text input and outputs a list of similar papers.

## Installation
 
See [the INK documentation](https://gitlab.coko.foundation/INK/ink-api) for detailed instructions for installing this plugin into an instance of INK.

`MoreLikeThisStep` requires an Elasticsearch index of [Semantic Scholar's Open Research Corpus](http://labs.semanticscholar.org/corpus/), named `scholar` with type `paper`. Set `ELASTICSEARCH_HOST` in the INK instance's `.env` file as appropriate (e.g. `127.0.0.1`).

## Testing 

```sh
bundle exec rspec spec
```
