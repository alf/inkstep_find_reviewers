# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'find_reviewers/version'

Gem::Specification.new do |spec|
  spec.name           = 'inkstep_find_reviewers'
  spec.version        = FindReviewers::VERSION
  spec.date           = '2016-10-28'
  spec.summary        = 'Find reviewers for articles'
  spec.description    = 'This gem contains steps for finding suitable reviewers for the input text.'
  spec.authors        = ['Alf Eaton']
  spec.email          = 'eaton.alf@gmail.com'
  spec.homepage       = 'https://gitlab.coko.foundation/alf/inkstep_find_reviewers'
  spec.license        = 'MIT'

  spec.executables    = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files     = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths  = %w(lib)

  spec.add_dependency 'elasticsearch'
  spec.add_dependency 'elasticsearch-dsl'
  
  spec.add_development_dependency 'bundler', '~> 1.3'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rake'

  spec.required_ruby_version = '~> 2.2'
  spec.files         = Dir.glob('{lib}/**/*') + %w(./README.md)
end
