require 'spec_helper'
require 'find_reviewers/ink_step/coko/more_like_this_step'

describe InkStep::Coko::MoreLikeThisStep do
  let(:target_file_name) { 'paper.txt' }
  subject { InkStep::Coko::MoreLikeThisStep.new(chain_file_location: temp_directory, position: 1) }

  let(:target_file) { File.join(Dir.pwd, 'spec', 'fixtures', 'files', target_file_name) }
  let!(:input_directory) { File.join(temp_directory, InkStep::Base::INPUT_FILE_DIRECTORY_NAME) }

  before do
    create_directory_if_needed(input_directory)
    FileUtils.cp(target_file, input_directory)
  end

  after :each do
    FileUtils.rm_rf(FileUtils.rm_rf(Dir.glob("#{temp_directory}/*")))
  end

  describe '#perform_step' do
    before do
      create_directory_if_needed(input_directory)
      FileUtils.cp(target_file, input_directory)
    end

    context 'when finding similar papers' do
      let(:target_file_name) { 'paper.txt' }

      it 'finds similar papers' do
        subject.perform_step

        output_file_path = File.new(File.join(subject.send(:working_directory), 'paper_mlt.json'), 'r')
        
        result = JSON.parse(File.read(output_file_path))
        
        expect(result['hits'].length).to eq 100
      end
    end
  end

  describe '#version' do
    specify do
      expect{subject.version}.to_not raise_error
    end
  end

  describe '#description' do
    specify do
      expect{subject.class.description}.to_not raise_error
    end
  end

  describe '#human_readable_name' do
    specify do
      expect{subject.class.human_readable_name}.to_not raise_error
    end
  end
end
